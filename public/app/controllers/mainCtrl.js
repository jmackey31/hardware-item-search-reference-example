angular.module('mainCtrl', [])
    .controller('mainController', function(
        $rootScope,
        $state,
        $stateParams,
        $window,
        $location,
        HttpService,
        $http,
        AuthService
    ) {
        var vm = this;
        vm.loggedIn = AuthService.loggedIn;
        vm.loginUser = function(user) {
            AuthService.login(user)
            .then(function(data) {
                alert("Login successfully");
            })
            .catch(function(err) {
                console.log(err);
            });
        }
        vm.vars = {
            siteMessage: false,
            siteMessageClass: false
        };

        // Array of hardware items that can be used to populate the mongodb database and the elasticsearch cluster
        var randomHardwareItemsArr = [
            {
                productName: 'Sledge Hammer',
                productDescription: 'This hammer has all of the destruction power you need in order to bring down any wall.',
                color: 'brown and gold',
                size: 'large',
                price: '35',
                inStoreItem: true,
                state: 'ca',
                city: 'hollywood',
                zipcode: '90210'
            },
            {
                productName: 'Hydralic Jack Stand',
                productDescription: 'Built for the toughest heavy duty truck frame or full size cargo truck.',
                color: 'stainless steel',
                size: 'large',
                price: '250',
                inStoreItem: true,
                state: 'ga',
                city: 'atlanta',
                zipcode: '48474'
            },
            {
                productName: 'Plunger',
                productDescription: 'For any minor plumbing issue.',
                color: 'black',
                size: 'small',
                price: '5',
                inStoreItem: false,
                state: 'ga',
                city: 'atlanta',
                zipcode: '34343'
            },
            {
                productName: 'Logging Saw',
                productDescription: 'Trees don\'t stand a chance with this thing.',
                color: 'yellow',
                size: 'medium',
                price: '499',
                inStoreItem: true,
                state: 'bristol',
                city: 'in',
                zipcode: '46516'
            },
            {
                productName: 'Sling Blade',
                productDescription: 'Description unavailable',
                color: 'green',
                size: 'small',
                price: '24',
                inStoreItem: true,
                state: 'ca',
                city: 'hollywood',
                zipcode: '90210'
            },
            {
                productName: 'Power Drill',
                productDescription: 'Who said that "Black and Decker" made the best power tools?  Give this one a try.',
                color: 'black',
                size: 'small',
                price: '130',
                inStoreItem: true,
                state: 'ga',
                city: 'atlanta',
                zipcode: '34343'
            }
        ];
        // console.log(randomHardwareItemsArr);

        // ----------------------------------------------------------------------------
        // popluate the database
        vm.addData = function() {
            randomHardwareItemsArr.forEach(function(item) {
                // console.log(item);
                var postInfo = {
                    obj: true,
                    uri: 'newhardwareitem',
                    hardwareItem: item
                };
                // console.log(postInfo);
                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        if (data.data.success) {
                          // when the data has been added run the getHardwareRecords function
                          vm.getHardwareRecords();
                        } else {
                            vm.vars.siteMessage = data.data.message;
                            vm.vars.siteMessageClass = 'alert alert-danger';
                        }
                    });
            });
        }
        // ----------------------------------------------------------------------------
        // Retrieve mongodb records to dispay on the data page
        vm.getHardwareRecords = function() {
            var getInfo = {
                singleEndPoint: true,
                uri: 'gethardwareitems'
            };
            // console.log(getInfo);
            HttpService.getRequest(getInfo)
                .then(function(data) {
                    if (data.data.success) {
                        vm.vars.siteMessage = 'Your records are in!';
                        vm.vars.siteMessageClass = 'alert alert-success';

                        // Found hardware items array
                        vm.foundHardwareItems = data.data.allharware;
                    } else {
                        vm.vars.siteMessage = 'Failure to get hardware items.  You may need to add some.';
                        vm.vars.siteMessageClass = 'alert alert-danger';
                    }
                });
        };
        vm.getHardwareRecords();

        // ----------------------------------------------------------------------------

        // console.log(vm);
});
