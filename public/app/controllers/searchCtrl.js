angular.module('searchCtrl', [])
    .controller('searchController', function(
        $rootScope,
        $scope,
        $http,
        $state,
        $stateParams,
        $window,
        $location,
        HttpService
    ) {
        var vm = this;

        vm.vars = {
            search: {
                city: 'hollywood',
                state: 'ca'
            },
            searchedTerm: false,
            currentCity: null,
            currentState: null
        };
        // ----------------------------------------------------------------------------
        // Get ES Search Results
        function getESSearch(searchObj) {
            console.log(searchObj);
            // Clear the messages var.
            vm.noReslutsMessage = false;
            vm.vars.paginationRequired = false;
            // --------------------------
            var postInfo = {
                obj: true,
                uri: 'hardwaresearch',
                searchTerm: searchObj.searchTerm,
                page: 1,
                city: vm.vars.search.city,
                state: vm.vars.search.state
            };
            console.log('Hardware search post info from your keystroke:');
            console.log(postInfo);
            if (postInfo.searchTerm) {
                HttpService.postRequest(postInfo)
                    .then(function(data) {
                        console.log(data);
                        if (data.data.success) {
                            // console.log(data);
                            if(data.data.requireLogin && data.data.requireLogin == true){
                                alert("Please login if you have account or signup.");
                                $state.go('login');
                            }
                            vm.returnedSearch = data.data.searchResults;
                            vm.vars.searchedTerm = data.data.searchterm;
                            console.log('vm.vars.searchedTerm');
                            console.log(vm.vars.searchedTerm);
                        } else {
                            vm.vars.searchedTerm = postInfo.searchTerm;
                            vm.noReslutsMessage = 'Nothing returned from your search';
                            vm.returnedSearch = false;
                        }
                    });
            } else {
                vm.vars.searchedTerm = 'No search term was provided.';
                vm.returnedSearch = false;
            }
        }
        // ----------------------------------------------------------------------------
        // Get search results
        vm.getSearchResults = function(event, searchObj) {
            if(event.which != 13)
                return;

            console.log(searchObj);

            // Make sure that the searchterm exists in the search object before letting the search run.
            if ((searchObj.searchTerm) &&
                (searchObj.searchTerm !== null) &&
                (searchObj.searchTerm !== undefined)) {
                getESSearch(searchObj);
            } else {
                vm.vars.searchedTerm = 'No search term was provided.';
                vm.returnedSearch = false;
            }
        };
        // ----------------------------------------------------------------------------
})
// Filter / Cut and limit words/strings.
.filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                        value = value.substr(0, lastspace);
                }
        }

        return value + (tail || ' …');
    };
});
