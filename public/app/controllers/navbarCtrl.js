angular.module('navbarCtrl', [])
    .controller('navbarController', function (
        $rootScope,
        $scope,
        $state,
        $http,
        $location,
        $window,
        HttpService,
        AuthService
    ) {

        var vm = this;

        vm.vars = {
            processing: true
        };

        // Main menu
        vm.mainMenu = [
            {
                'title': 'Home',
                'link': 'home'
            },
            {
                'title': 'Current Data',
                'link': 'populate-data'
            },
            {
                'title': 'Generate QR Codes',
                'link': 'generate-qr-codes'
            }
        ];

        if(AuthService.loggedIn == false){
          vm.mainMenu.push(
            {
                'title': 'Login',
                'link': 'login'
            },
            {
                'title': 'Sign Up',
                'link': 'signup'
            });
        };
        vm.signUpUser = function() {
            vm.processing = true;
        };

        vm.loginUser = function() {
            vm.processing = true;
        };

        vm.logOutUser = function() {
          AuthService.logout();
          $state.go('login');
        };
        // console.log(vm);
        // ----------------------------------------------------------------------------
  })
  // Filter / Cut and limit words/strings.
  .filter('cut', function () {
      return function (value, wordwise, max, tail) {
          if (!value) return '';

          max = parseInt(max, 10);
          if (!max) return value;
          if (value.length <= max) return value;

          value = value.substr(0, max);
          if (wordwise) {
                  var lastspace = value.lastIndexOf(' ');
                  if (lastspace != -1) {
                          value = value.substr(0, lastspace);
                  }
          }

          return value + (tail || ' …');
      };
  });
