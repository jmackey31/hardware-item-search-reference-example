angular.module('app.routes', ['ngRoute'])
    .config(function(
        $stateProvider,
        $urlRouterProvider,
        $locationProvider) {

        var headerContent = {
            controller: 'navbarController',
            controllerAs: 'nav',
            templateUrl: 'app/views/header-view.html'
        };

        $stateProvider
            // Home page state
            .state('home', {
                url: '/',
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                      controller: 'searchController',
                      controllerAs: 'search',
                      templateUrl: 'app/views/main-search-view.html'
                    }
                }
            })
            // Search page state
            .state('my-profile', {
                url: '/my-profile',
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                      controller:   'mainController',
                      controllerAs: 'main',
                      templateUrl: 'app/views/my-profile-redirect.html'
                    }
                }
            })
            // Item Profile state
            .state('item-profile', {
                url: '/item-profile/:id',
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                      templateUrl: 'app/views/item-profile-view.html'
                    }
                }
            })
            // General login state
            .state('login', {
                url: '/login',
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                        templateUrl:    'app/views/login-view.html',
                        controller:     'mainController',
                        controllerAs:   'main'
                    }
                }
            })
            // Signup user
            .state('signup', {
                url: '/signup',
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                        templateUrl:    'app/views/signup-view.html',
                        controller:     'mainController',
                        controllerAs:   'signup'
                    }
                }
            })
            // Signup user
            .state('populate-data', {
                url: '/populate-data',
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                        templateUrl:    'app/views/add-data-view.html',
                        controller:     'mainController',
                        controllerAs:   'main'
                    }
                }
            })
            // Generate QR Codes
            .state('generate-qr-codes', {
                url: '/generate-qr-codes',
                views: {
                    'headercontent@': headerContent,
                    'webcontent@': {
                        templateUrl:    'app/views/generate-qr-codes.html',
                        controller:     'mainController',
                        controllerAs:   'main'
                    }
                }
            });

        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);
    });
