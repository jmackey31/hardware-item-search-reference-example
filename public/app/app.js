angular.module('hardwareApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'app.routes',
    'mainCtrl',
    'searchCtrl',
    'navbarCtrl',
    'httpService',
    'authService'
])
// application configuration to integrate token into requests
.config(function() {

});
