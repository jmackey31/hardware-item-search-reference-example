// 'use strict';

angular.module('httpService', [])
  .factory('HttpService', function($http) {

    var httpFactory = {};

    // Get Stuff
    httpFactory.getRequest = function(getInfo) {
        if (getInfo.singleEndPoint) {
            var endpoint = '/api/' + getInfo.uri;
            return $http.get(endpoint);
        } else if (getInfo.paramString) {
            var endpoint = '/api/' + getInfo.uri + '/' + getInfo.param;
            return $http.get(endpoint);
        }
    };

    // Post Stuff
    httpFactory.postRequest = function(postInfo) {
        if (postInfo.obj) {
            return $http.post('/api/' + postInfo.uri + '/', postInfo);
        } else if (postInfo.paramString) {
            var endpoint = '/api/' + postInfo.uri + '/' + postInfo.param;
            return $http.post(endpoint);
        } else if (postInfo.paramObj) {
            return $http.post('/api/' + postInfo.uri + '/' + postInfo.param + '/', postInfo);
        }
    };

    // return our entire httpFactory object
    return httpFactory;

  });
