// 'use strict';

angular.module('authService', [])
  .factory('AuthService', function($http) {

    var authFactory = {};
    authFactory.loggedIn = false;
    // Get Stuff
    authFactory.login = function(user) {
        return $http.post('/api/login', user)
        .then(function(response){
        	if(response.data.success == true){
        		authFactory.loggedIn = true;
        		return response.data;
        	}
        	else
            	throw response;
        });
    };

    authFactory.logout = function() {
        authFactory.loggedIn = false;
        return false;
    };
    // return our entire httpFactory object
    return authFactory;

  });
