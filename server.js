'use strict';

var express           = require('express'),
    app               = express(),
    bodyParser        = require('body-parser'),
    morgan            = require('morgan'),
    cookieParser      = require('cookie-parser'),
    mongoose          = require('mongoose'),
    mongoosePaginate  = require('mongoose-paginate'),
    dotenv            = require('dotenv').config(),
    config            = require('./config'),
    path              = require('path'),
    methodOverride    = require('method-override'),
    session           = require('express-session');

app.use(methodOverride());

// APP CONFIGURATION
// use body parser so we can grap information from POST requests
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json());

app.use(cookieParser());

// configure our app to handle CORS requests
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, \ Authorization');
    next();
});

// log all requests to the console
app.use(morgan('dev'));

// use session
app.use(session({
  secret: 'session secret key',
  resave: false,
  saveUninitialized: true
}));

// connect to the database
mongoose.connect(config.database);

// set static files location
// used for requests that our frontend will make
app.use(express.static(__dirname + '/public'));

// API ROUTES -------------------------
var apiRoutes = require('./app/routes/api')(app, express);
app.use('/api', apiRoutes);

// MAIN CATCHALL ROUTE ----------------
// SEND USERS TO THE FRONTEND ---------
// has to be registered after API ROUTES
app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/app/views/index.html'));
});

// START THE SERVER
app.listen(config.port);
console.log('View the project on: http://localhost:' + config.port);
