# Hardware Store Item Search Example

### What is this repository for? ###
An "EXAMPLE / SAMPLE" hardware store product item search.  All code and data examples in this project are for a fictional online hardware store catalog.

## How to run this project ###
* run these commands from the root of the project directory
* run 'npm install' to install the node_modules package dependencies
* run 'bower install' to install the bower_components package dependencies
* To start the project using node, execute: "node server.js" or any node server of your choice.
* To clean the "dist" directory run "gulp clean"

### Technologies utilized ###
* Angularjs version 1
* Bootstrap
* jQuery
* lodash
* Gulp
* NPM
* Node.js
* Bower.js

### Other Notes ###
* If you have any questions or concerns, please contact the person who gave you this project link.
