module.exports = {
    'port': process.env.PORT || process.env.URL_PORT,
    'database': process.env.DB_NAME,
    'secret': process.env.DB_SECRETE
};
