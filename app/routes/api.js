'use strict';

var Hardware          = require('../models/hardware'),
    GuestLogging      = require('../models/guestlogging'),
    config            = require('../../config'),
    superSecret       = config.secret,
    fs                = require('fs'),
    async             = require('async'),
    crypto            = require('crypto'),
    bcrypt            = require('bcrypt-nodejs'),
    qr                = require('qr-image'),
    backup            = require('mongodb-backup');

console.log('Database connection string: ' + config.database);

var elasticsearch = require('elasticsearch');
console.log('API Elasticsearch plugin in use.');
var client = new elasticsearch.Client({
    host: process.env.ELASTICSEARCH_PLUGIN_DOMAIN_HOST
    // host: 'localhost:9200'
    // log: 'trace'
});

client.ping({
    // ping usually has a 3000ms timeout
    requestTimeout: Infinity
}, function (error) {
    if (error) {
        console.trace('elasticsearch cluster is down!');
        console.log(error)
    } else {
        console.log('All is well');
    }
});

module.exports = function(app, express) {

    // get an instance of the express router
    var apiRouter = express.Router();


    // ------------------------------------------------------------------------------
    // Run a database backup with this endpoint
    apiRouter.route('/jmbackupdb')
        .get(function(req, res) {
            console.log('Live from the db backup route.');

            backup({
                // uri: 'uri', // mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
                uri: process.env.DB_NAME, // mongodb://<dbuser>:<dbpassword>@<dbdomain>.mongolab.com:<dbport>/<dbdatabase>
                // root: __dirname, // write files into this dir
                root: './db_backups', // write files into this dir
                // root: __dirname + '/db_backups', // write files into this dir
                callback: function(err) {
                    if (err) {
                        console.log(err);
                        return res.json({
                            success: false,
                            message: 'Could not backup this database.',
                            err: err
                        });
                    } else {
                        console.log('finish');
                        return res.json({
                            success: true,
                            message: 'Database is now backed up and placed in the backup directory.',
                        });
                    }
                }
            });
        });

    // ------------------------------------------------------------------------------
    // Generate a QR Code
    apiRouter.route('/makeaqrcode')
        .get(function(req, res) {
            console.log('Generate a QR Code');

            var qr_svg = qr.image('I like trees!', { type: 'svg' });
            var fileName = 'newqrcode.svg';
            qr_svg.pipe(require('fs').createWriteStream('./tempqrcodefiles/'+fileName));

            var svg_string = qr.imageSync('Do stuff!', { type: 'svg' });
            var finalQR = svg_string;
            var filePath = './tempqrcodefiles/'+fileName;
            // Remove the svg file from the file system
            fs.unlinkSync(filePath);
            console.log('Temporary qr file just got zapped away!');
            return res.json({
                success: true,
                message: 'Code is in!',
                finalQR: finalQR
            });
        });

    // ------------------------------------------------------------------------------
    // Log in current user
    apiRouter.route('/login')
        .post(function(req, res) {
            console.log("username", req.body.username);
            console.log("password", req.body.password);
            console.log("User logged in");

            // User authentication system
            // User.find({}, {'_id': false})
            //     .exec(function(err, allharware) {
            //     });
            var user = { username: req.body.username };
            req.session.user = user;
            console.log(req.session.user);
            return res.json({
                success: true,
                user: user
            });
        });
    // ------------------------------------------------------------------------------
    // Create a new hardware item
    apiRouter.route('/newhardwareitem')
        .post(function(req, res) {
            console.log('Req body here:');
            console.log('User here', req.session.user);

            console.log(req.body);
            console.log('-------------------------');
            // create a new instance of the Hardware schema
            var hardware = new Hardware();
            // set the hardwares information (comes from the request)
            if (req.body.hardwareItem.productName) hardware.productName                    = req.body.hardwareItem.productName.toLowerCase();
            if (req.body.hardwareItem.productDescription) hardware.productDescription      = req.body.hardwareItem.productDescription.toLowerCase();
            if (req.body.hardwareItem.color) hardware.color                                = req.body.hardwareItem.color.toLowerCase();
            if (req.body.hardwareItem.size) hardware.size                                  = req.body.hardwareItem.size.toLowerCase();
            if (req.body.hardwareItem.price) hardware.price                                = req.body.hardwareItem.price;
            if (req.body.hardwareItem.inStoreItem) hardware.inStoreItem                    = req.body.hardwareItem.inStoreItem;
            if (req.body.hardwareItem.state) hardware.state                                = req.body.hardwareItem.state.toLowerCase();
            if (req.body.hardwareItem.city) hardware.city                                  = req.body.hardwareItem.city.toLowerCase();
            if (req.body.hardwareItem.zipcode) hardware.zipcode                            = req.body.hardwareItem.zipcode;

            // Programatically create a item code
            function createItemCode() {
                var getTimeNow = Date.parse(new Date());
                var randNum = Math.random().toString();
                var slicedNum = randNum.slice(3,9);

                var newItemCode = 'item'+getTimeNow+slicedNum;
                return newItemCode;
            }
            hardware.itemCode                                                                   = createItemCode();

            console.log('New constructed hardware item.');
            console.log(hardware);

            // save the hardware and check for errors
            hardware.save(function(err) {
                if (err) {
                    return res.json({
                        success: false,
                        err: err,
                        message: 'Something went wrong while creating your hardware item.'
                    });
                } else {
                    return res.json({
                        success: true,
                        message: 'Your hardware item has been created and indexed!'
                    });
                }
            });
        });
    // ------------------------------------------------------------------------------
    // Get current hardware items in mongodb database
    apiRouter.route('/gethardwareitems')
        .get(function(req, res) {
            Hardware.find({}, {'_id': false})
                .exec(function(err, allharware) {
                    if (err) {
                        return res.json({
                            success: true,
                            err: err,
                            message: 'Something went wrong while getting all hardware'
                        });
                    } else if (allharware.length > 0) {
                        return res.json({
                            success: true,
                            message: 'All hardware is in!',
                            totalCount: allharware.length,
                            allharware: allharware
                        });
                    } else {
                        return res.json({
                            success: false,
                            message: 'Something went wrong while getting all hardware'
                        });
                    }
                });
        });
    // ------------------------------------------------------------------------------
    apiRouter.route('/hardwaresearch')
        .post(function(req, res) {
            var ipAddress = req.connection.remoteAddress.replace(/\//g, '.');

            console.log('Your reqeust body: ' );
            console.log(req.body.searchTerm);
            var sendResult = function(loginfo){

                // Anonymouse user can log still search.
                var terms             = req.body.searchTerm.toLowerCase(),
                    page              = req.body.page,
                    pages             = null,
                    nextStart         = null,
                    from              = 0,
                    limit             = 10,
                    total             = null,
                    pageRange         = [],
                    searchPagi        = {},
                    currentCity       = req.body.city,
                    currentState      = req.body.state,
                    queryFilter       = {};

                // ------------------------------

                console.log('Start City and State logging ----------');
                console.log('City: ' + currentCity);
                console.log('State: ' + currentState);
                console.log('---------- End City and State logging');

                // Run the Elasticsearch Query
                client.search({
                    index: 'hardwares',
                    type: 'hardware'
                    // body: {
                    //     query : {

                    //       constant_score : {
                    //         filter : {
                    //           bool : {
                    //             should : [
                    //               { fuzzy : { itemCode : terms }},
                    //               { fuzzy : { productName : terms }},
                    //               { fuzzy : { productDescription : terms }},
                    //               { fuzzy : { color : terms }},
                    //               { fuzzy : { size : terms }}
                    //             ],
                    //             must_not : { term : { inStoreItem : false } },
                    //             must : [
                    //               { match: { city: currentCity }},
                    //               { match: { state: currentState }}
                    //             ]
                    //           }
                    //         }
                    //       }
                    //     },
                    //     from: from,
                    //     size: limit
                    // }
                })
                .then(function (resp) {
                    // console.log(resp.hits.hits);
                    var foundResults = resp.hits.hits,
                        newResultsArray = [];
                    foundResults.forEach(function(item) {
                        var newObj = {
                            itemCode: item._source.itemCode,
                            productName: item._source.productName,
                            productDescription: item._source.productDescription,
                            color: item._source.color,
                            size: item._source.size,
                            price: item._source.price,
                            inStoreItem: item._source.inStoreItem,
                            city: item._source.city,
                            state: item._source.state,
                            zipcode: item._source.zipcode
                        };
                        newResultsArray.push(newObj);
                    });
                    console.log('newResultsArray');
                    console.log(newResultsArray.length);
                    function sendSearchResults() {
                        if (newResultsArray.length === 0) {
                            console.log('Nothing found dude');
                            return res.json({
                                success: false,
                                searchterm: terms,
                                searchResults: newResultsArray,
                                message: 'No results found in your search.'
                            });
                        } else {
                            // Create the page range
                            var foundDocs = resp.hits.total;
                            var pagesForRange = foundDocs / limit;
                            var newResultsArrayLength = newResultsArray.length;

                            for (var i = 1; i < pagesForRange + 1; i++) {
                                pageRange.push(i);
                            }

                            searchPagi = {
                                totalDocs: resp.hits.total,
                                toDisplay: newResultsArrayLength,
                                nextStart: newResultsArrayLength + 1,
                                pages: pageRange.length,
                                page_range: pageRange,
                            };

                            return res.json({
                                success: true,
                                message: 'Hardware search results are in!',
                                searchterm: terms,
                                searchResults: newResultsArray,
                                pagination: searchPagi,
                                leftSearchCount: loginfo ? process.ENV - loginfo.visit : -1
                            });
                        }
                    }
                    // Fire the "sendSearchResults" function.
                    sendSearchResults();
                }, function (err) {
                    console.trace(err.message);
                    return res.json({
                        success: false,
                        err: err,
                        message: 'Something went wrong while executing your search.'
                    });
                });
            }
            if(req.session.user)
                return sendResult();

            GuestLogging.findOne({ipAddress: ipAddress, sessionID: req.session.id})
            .exec(function(err, loginfo) {
                if(err)
                    return res.json({
                        success: true,
                        err: err,
                        message: 'Something went wrong while getting log information'
                    });
                if(loginfo){
                    loginfo.visit++;
                    if(loginfo.visit > process.env.SEARCH_LIMIT)
                        return res.json({
                            success: true,
                            requireLogin: true,
                            message: 'Something went wrong while getting log information'
                        });
                    loginfo.save(function(err){
                        if(err)
                            return res.json({
                                success: true,
                                err: err,
                                message: 'Something went wrong while getting log information'
                            });
                        sendResult(loginfo);
                    })
                }
                else{
                    new GuestLogging({ipAddress: ipAddress, visit: 1, sessionID: req.session.id})
                    .save(function(err, loginfo){
                        if(err)
                            throw new Error("error");
                        sendResult(loginfo);
                    })
                }
            });
        });

    // --------------------------------------------------------------------------

    apiRouter.get('/', function(req, res) {
        res.json({ message: 'Find all of the hardware you may want.'});
    });
    // ------------------------------------------------------------------------------

    return apiRouter;

};
