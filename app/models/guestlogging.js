'use strict';

var mongoose          = require('mongoose'),
    mongoosastic      = require('mongoosastic'),
    mongoosePaginate  = require('mongoose-paginate'),
    Schema            = mongoose.Schema;

var GuestLoggingSchema = new Schema({
    ipAddress: { type: String },
    sessionID: { type: String },
    visit: { type:Number },
    created: { type: Date },
    updated: [{ type: Date }]
});

// return the model
var GuestLogging = mongoose.model('GuestLogging', GuestLoggingSchema);

module.exports = mongoose.model('GuestLogging', GuestLoggingSchema);
