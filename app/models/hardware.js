'use strict';

var mongoose          = require('mongoose'),
    mongoosastic      = require('mongoosastic'),
    mongoosePaginate  = require('mongoose-paginate'),
    Schema            = mongoose.Schema;

var HardwareSchema = new Schema({
    itemCode: { type: String, es_indexed: true },
    productName: { type: String, es_indexed: true },
    productDescription: { type: String, es_indexed: true },
    color: { type: String, es_indexed: true },
    size: { type: String, es_indexed: true },
    price: { type: Number, es_indexed: true },
    inStoreItem: { type: Boolean, default: false, es_indexed: true },
    state: { type: String, es_indexed: true },
    city: { type: String, es_indexed: true },
    zipcode: { type: Number, es_indexed: true },
    created: { type: Date },
    updated: [{ type: Date }]
});

console.log('"HardwareSchema" is utilizing Mongoosastic and Elasticsearch.');
HardwareSchema.plugin(mongoosastic, {
    host: process.env.ELASTICSEARCH_DOMAIN_HOST,
    port: process.env.ELASTICSEARCH_DOMAIN_PORT,
    protocol: process.env.ELASTICSEARCH_DOMAIN_HOST_PROTOCAL
    // auth: process.env.ELASTICSEARCH_AUTH_CREDS,
    // log: 'trace'
});

HardwareSchema.plugin(mongoosePaginate);

// return the model
var Hardware = mongoose.model('Hardware', HardwareSchema);

Hardware.createMapping(function(err, mapping) {
    if (err) {
        console.log('error creating mapping (you can safely ignore this)');
        console.log(err);
    } else {
        console.log('mapping created!');
        console.log(mapping);
    }
});

module.exports = mongoose.model('Hardware', HardwareSchema);
